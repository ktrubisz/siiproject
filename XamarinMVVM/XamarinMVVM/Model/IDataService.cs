﻿using System;

namespace XamarinMVVM.Model
{
    public interface IDataService
    {
        void GetData(Action<DataItem, Exception> callback);
    }
}