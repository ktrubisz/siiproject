﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class CallWaiterModel
    {
        public string IdCall { get; set; }
        public string IdClient { get; set; }
        public string IdTable { get; set; }
        public string Description { get; set; }
        public bool IsResponsed { get; set; }
    }
}