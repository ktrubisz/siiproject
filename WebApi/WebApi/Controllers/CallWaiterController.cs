﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.AspNetCore.Routing;
using WebApi.DB;
using WebApi.Models;

namespace WebApi.Controllers
{
    [RoutePrefix("api/waiter")]
    public class CallWaiterController : ApiController
    {
        [Route("AddCall")]
        public IHttpActionResult AddCall(CallWaiterModel callWaiterModel)
        {
            try
            {
                callWaiterModel.IdCall = Guid.NewGuid().ToString();
                HardcodedDB.AddCallWaiter(callWaiterModel);
                return Ok(callWaiterModel.IdCall);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [Route("CallAproved")]
        public IHttpActionResult CallAproved(CallWaiterModel callWaiterModel)
        {
            try
            {
                var callWaiter = HardcodedDB.ExistCallWaiter(callWaiterModel);
                if (callWaiter != null)
                {
                    callWaiter.IsResponsed = true;
                    return Ok();
                }
                return BadRequest("Wrong idCall");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [Route("GetCalls")]
        public IEnumerable<CallWaiterModel> GetCalls()
        {
            return HardcodedDB.GetCallslWaiter();
        }

        [Route("ClearCalls")]
        [HttpGet]
        public IHttpActionResult ClearCalls()
        {
            try
            {
                HardcodedDB.ClearDB();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}