﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.DB
{
    public static class HardcodedDB
    {
        public static List<CallWaiterModel> CallWaiterModels = new List<CallWaiterModel>();

        public static void AddCallWaiter(CallWaiterModel callWaiterModel)
        {
            CallWaiterModels.Add(callWaiterModel);
        }

        public static CallWaiterModel ExistCallWaiter(CallWaiterModel callWaiterModel)
        {
            return CallWaiterModels.Find(m => m.IdCall == callWaiterModel.IdCall);
        }

        public static List<CallWaiterModel> GetCallslWaiter()
        {
            return CallWaiterModels;
        }

        public static void ClearDB()
        {
            CallWaiterModels.Clear();
        }
    }
}